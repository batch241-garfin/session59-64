
import '../App.css';
import { useState, useEffect, useContext } from 'react';
import { Form, Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';

import UserContext from '../UserContext';

import Swal from 'sweetalert2';

export default function CourseView() {

	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	const {productId} = useParams();
	
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(0);

	const addCart = (productId, quantity) => {

		fetch(`${process.env.REACT_APP_API_URL}/users/createOrder`, {

			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if(data === true){
				
				Swal.fire({
				  title: "Successfully Added!",
				  icon: "success",
				  text: "You have successfully added this to your cart."
				})
				navigate("/products");
				} else {
				  Swal.fire({
				  title: "Something went wrong",
				  icon: "error",
				  text: "Please try again."
				})
			}
		})
	}

	useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/getProduct`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [productId])


	return (

		<Container className="marginTopv">
			<Row className="mt-5">
				<Col lg={{span: 6, offset:3}} >
					<Card className="activeCards detailCard">
					      <Card.Body className="text-center">
					        <Card.Title className="CardTitle mt-4 fontTitle">{name}</Card.Title>
					        <Card.Subtitle className="mt-3 b">Description:</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle className="b">Price:</Card.Subtitle>
					        <Card.Text className="priceBtn">PhP {price}.00</Card.Text>

					        <Form>
					        <>
					        <label for="quantity" className="b">Quantity : </label>
					        <input type="number" id="quantity" name="quantity" min="1" onChange={e => setQuantity(e.target.value)} required />
					        </>
					        </Form>
					        {/*<Form>
					        <Form.Group controlId="quantity">
				                <Form.Label>Quantity:</Form.Label>
				                <Form.Control 
					                type="text"
					                min="1"
				                    value={quantity}
				                    onChange={e => setQuantity(e.target.value)}
					                required
				                />
				            </Form.Group>

					        </Form>*/}

					        <Button className="marginLeftNew mt-3 mr-5 mb-2" as={Link} to="/products">Go Back</Button>
					        
					        {
					        	(user.isAdmin === false) ?
					        		<Button className="marginLeftNew mt-3 mb-2 succBtn" onClick={() => addCart(productId,quantity)} >Add to Cart</Button>
					        		:
					        		<Button className="btn errBtn mt-3 marginLeftNew mb-2" as={Link} to="/products">Unauthorized user</Button>
					        }

					      </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

	)
}