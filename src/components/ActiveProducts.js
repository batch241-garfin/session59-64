
import { Link } from 'react-router-dom';

import {Card, Col, Row } from 'react-bootstrap';
import Button from 'react-bootstrap/Button';

export default function ActiveProducts({product}) {

  const { name, description, price, _id } = product;
  

  return (
    
    <Col className="container-fluid bg-success ">
        <Row className="cardSize col-md-4 col-sm-12 m-auto mb-3 justify-content-center">
    <Card className="activeCards marginTop mt-3 ml-5 mr-5">
      <Card.Body className="">
        <Card.Title className="CardTitle mt-3 text-center">- {name} -</Card.Title>
        <Card.Subtitle className="mt-3 b">Description:</Card.Subtitle>
        <Card.Text className="">
          {description}
        </Card.Text>
        <Card.Subtitle className="b">
          Price:
        </Card.Subtitle >
        <Card.Text className="priceBtn mt-2">
          PhP {price}.00
        </Card.Text>
        <Button className="bg-primary gradBtn" as={Link} to={`/activeProducts/${_id}`}>Details</Button>
      </Card.Body>
    </Card>
    </Row>
    </Col>
  );
}