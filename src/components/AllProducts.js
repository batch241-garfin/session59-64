
import '../App.css';


import { Link } from 'react-router-dom';

import { Button, Row, Col, Card } from 'react-bootstrap';

export default function AllProducts({products}) {


  const { name, description, price, _id } = products;

  return (
    <>
    <Col className="container-fluid bg-success">
        <Row className="cardSize col-md-4 m-auto mb-3">
            <Card className="activeCards detailCard ">
            <Card.Body className="text-center">
            <Card.Title className="CardTitle mt-4 fontTitle">{name}</Card.Title>
            <Card.Subtitle className="mb-2 b">Description:</Card.Subtitle>
            <Card.Text>
                {description}
            </Card.Text>
            <Card.Subtitle className="b">
              Price:
            </Card.Subtitle>
            <Card.Text className="priceBtn">
              PhP {price}.00
            </Card.Text>

            <Button className="detailBtns" as={Link} to={`/products/${_id}`}>EDIT</Button>
            </Card.Body>
            </Card>
        </Row>
    </Col>
    </>
  );
}