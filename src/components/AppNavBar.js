
import '../App.css';
import { useContext } from 'react';
import { Link, NavLink } from 'react-router-dom'

import UserContext from "../UserContext";
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

export default function AppNavbar() {

  const { user } = useContext(UserContext)

  return (

    <Navbar expand="lg" className="mb-5 navibar">
      
        <Navbar.Brand as={Link} to="/" className="navBarLogo">Lehmour</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ml-auto">
            <Nav.Link as={NavLink} to="/" className="navi textCenter">Home</Nav.Link>
            <Nav.Link as={NavLink} to="/products" className="navi textCenter">Products</Nav.Link>

            {(user.id !== null) ?
              <>
              {(user.isAdmin === true) ?
                <>
                <Nav.Link as={NavLink} to="/dashboard" className="navi textCenter">Dashboard</Nav.Link>
                <Nav.Link as={NavLink} to="/logout" className="navi textCenter">Logout</Nav.Link>
                </>
                :
                <>
                <Nav.Link as={NavLink} to="/cart" className="navi textCenter">Cart</Nav.Link>
                <Nav.Link as={NavLink} to="/logout" className="navi textCenter">Logout</Nav.Link>
                </>
              }
              </>
              :
              <>
              <Nav.Link as={NavLink} to="/register" className="navi textCenter">Register</Nav.Link>
              <Nav.Link as={NavLink} to="/login" className="navi textCenter">Login</Nav.Link>
              </>
            }
            
          </Nav>
        </Navbar.Collapse>
     
    </Navbar>
  );
}