
import { Button, Row, Col } from 'react-bootstrap';

import { Link } from 'react-router-dom';

export default function Banner({data}) {

    const { title, content, destination, subTitle, label } = data;

    return (

        <Row className="mt-5 pt-3 homeBg">
            <Col className="p-5 mTop mLeft">
            <h1 className="b txtC p-2 fontGlock">{title}</h1>
            <h5 className="txtC p-2 fontNiconne">{content}</h5>
            <Button className="txtC mt-1 ml-auto p-2 bb" as={Link} to={destination}>{label}</Button>
            <h2 className="b txtC mt-5 p-2 fontGaj">{subTitle}</h2>
            </Col>
            {/*<img src="../warcr.jpeg" alt="React Image" />*/}
        </Row>
    )


}