
import '../App.css';

import { useState, useEffect } from 'react';

import { Button, Row, Col, Form } from 'react-bootstrap';

import { Link } from 'react-router-dom';

import AllProducts from '../components/AllProducts';


export default function LabelDashboard({data}) {

    const [searchInput, setSearchInput] = useState('');
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState(0);
    const [isActive, setIsActive] = useState(false);

    const [products, setProducts] = useState([]);
    const [searchProd, setSearchProd] = useState([]);

    const { title, destination2, search, add } = data;

    const searchBar = (searchText) => {

        setIsActive(true);

        fetch(`${process.env.REACT_APP_API_URL}/products/search`, {

            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: searchText
            })
        })
        .then(res => res.json())
        .then(data =>
        {
            setName(data[0]);
            setDescription(data[1]);
            setPrice(data[2]);

            const product = {
                name: name,
                description: description,
                price: price,
            }; 

            setSearchProd(data.map(product => {
             return (
                 <AllProducts key={product.id} products ={product} />
             )
            })) 
        });
    }

    const allProducts = () => {

        setIsActive(false)

        fetch(`${process.env.REACT_APP_API_URL}/products/allProducts`, {

            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            setProducts(data.map(product => {
                return (
                    <AllProducts key={product.id} products ={product} />
                )
            }))
        })
    };

    useEffect(() => {

        if(isActive)
        {
            setIsActive(true);
        }
        else
        {
            setIsActive(false);
        }
    }, [isActive]);

    return (
        <>
        <Col className="marginTopv p-1">
            <Row >
            <h1 className="glock b">{title}</h1>
            </Row>
        </Col>
        <Row className="mb-4 row-md-6 bgblue p-2">
            <Col className="pt-3 searchbgS">
            <Form.Control className="justify-content-center"
                    type="text" 
                    placeholder="Search. . ."
                    value={searchInput}
                    onChange={e => setSearchInput(e.target.value)}
                />
            </Col>
            <Col className="p-3 b">
            <Button className="marginLeftSearch gradBtn b" onClick={() => searchBar(searchInput)}>{search}</Button>
            <Button className="mLeft gradBtn b" onClick={() => allProducts()}>All</Button>
            <Button className="marginLeftNew gradBtn b" as={Link} to={destination2}>{add}</Button>
            </Col>
        </Row>   
        
            {
                (isActive) ?
            <>
            {searchProd}
            </>
            :
            <>
            {products}
            </>
            }
            </>
    )


}