
import '../App.css';

import { useState, useEffect, useContext } from 'react';

import { Button, Row, Col, Form } from 'react-bootstrap';

import UserContext from '../UserContext';

import { useNavigate } from 'react-router-dom';

import Swal from 'sweetalert2';


export default function TotalAmount() {

    // const {user} = useContext(UserContext);

    // const [searchInput, setSearchInput] = useState('');
    // const [name, setName] = useState('');
    // const [description, setDescription] = useState('');
    // const [price, setPrice] = useState(0);
    // const [isActive, setIsActive] = useState(false);
   	const [total, setTotal] = useState(0);
   	const {user} = useContext(UserContext);
   	const navigate = useNavigate();

    // const [products, setProducts] = useState([]);
    // const [searchProd, setSearchProd] = useState([]);

    const displayTotal = `₱ ${total}.00`

    const checkOut = () => {

    	Swal.fire(
        {
            title: "Checkout Successful",
            icon: "success",
            text: "Your Parcel is on the way!"
        })
        navigate("/products");
    }

    useEffect(() => {

        fetch(`${process.env.REACT_APP_API_URL}/users/${user.id}/checkOut`, {

			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data =>
		{
			setTotal(data);
		});

    }, [user.id]);

    return (
        <>
        <Col className="marginTopv p-1">
            <Row className="mb-1  bgblue p-2">
            <Col className="p-3 searchbg row-lg-3 offset-lg-9 totalBg borderRad">
            <h4>Total Amount:</h4>
            <Form.Control className="txtCenter priceBtn fontSubTitle"
                    type="text"
                    placeholder="₱" 
                    disabled="true"
                    value={displayTotal}
                />
                <Button className="succBtns xxx mt-3" onClick={() => checkOut()}>Checkout</Button>
            </Col>
            
        </Row>
        </Col>
        
        </>
    )
}