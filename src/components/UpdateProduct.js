import { useState, useEffect, useContext } from 'react';

import { Form, Button } from 'react-bootstrap';

import { useParams, Link, useNavigate, Navigate } from 'react-router-dom';

import UserContext from '../UserContext';

import Swal from 'sweetalert2';

export default function UpdateProduct(product) {

	const {user} = useContext(UserContext);
	const navigate = useNavigate();
	const {productId} = useParams();
	
	const [name1, setName] = useState('');
	const [description1, setDescription] = useState('');
	const [price1, setPrice] = useState(0);
	const [isAble, setAble] = useState(false);
	const [isDisable, setDisable] = useState('');

	const update = (productId) => {
		console.log(productId)

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/updateProduct`, {

			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name1,
				description: description1,
				price: price1
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if(data === true){

				setName(data.name)
				setDescription(data.description)
				setPrice(data.price)

				
				Swal.fire({
				  title: "Successfully Updated",
				  icon: "success",
				  text: "You have successfully updated this product."
				})
				navigate("/dashboard");
				} else {
				  Swal.fire({
				  title: "Something went wrong",
				  icon: "error",
				  text: "Please try again."
				})
			}
		})
	}

	const able = (isAble) => {

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/updateActive`, {

			method: 'PATCH',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: !isAble
			})
		})
		.then(res => res.json())
		.then(data =>
		{
			if (isAble === false)
			{
				setAble(true)
			}
			else
			{
				setAble(false)
			}
		});

	}

	useEffect(() => {
            
			fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/getProduct`)
			.then(res => res.json())
			.then(data => {

				console.log(data);

				setName(data.name);
				setDescription(data.description);
				setPrice(data.price);
				setAble(data.isActive)
			})

			if (isAble)
			{
				setDisable('Disable')
			}
			else
			{
				setDisable('Enable')
			}

    }, [productId, isAble, isDisable]);

	return (
		
		(user.isAdmin === false || user.id === null) ?
        <Navigate to="/*"/>
        :
		<>
		<Form className="regForm p-5">
            <Form.Group controlId="prodName">
                <Form.Label className="b">Name:</Form.Label>
                <Form.Control 
	                type="text" 
	                placeholder={name1}
                    value={name1}
                    onChange={e => setName(e.target.value)}
	                required
                />
            </Form.Group>

            <Form.Group controlId="prodDescription">
                <Form.Label className="b mt-1">Description:</Form.Label>
                <Form.Control 
	                type="text" 
	                placeholder={description1}
                    value={description1}
                    onChange={e => setDescription(e.target.value)}
	                required
                />
            </Form.Group>

            <Form.Group controlId="prodPrice">
                <Form.Label className="b mt-1">Price:</Form.Label>
                <Form.Control 
	                type="text" 
	                placeholder={price1}
                    value={price1}
                    onChange={e => setPrice(e.target.value)}
	                required
                />
            </Form.Group>

            {
        	(user.id !== null) ?
        	<>
        	<Button className="marginLeftSearch m-2 mt-3 detailBtns" as={Link} to="/dashboard">Go Back</Button>
	        	{
	        	(isAble === false) ?
	    		<Button className="mt-3 m-2 gradBtn" onClick={() => able(isAble)} >{isDisable}</Button>
	    		:
	    		<Button className="mt-3 m-2 errBtn" onClick={() => able(isAble)} >{isDisable}</Button>
	    		}
    		<Button className="mt-3 m-2 succBtn" onClick={() => update(productId)} >Update</Button>
    		</>
    		:
    		<Button className="btn btn-danger mt-3 m-2" as={Link} to="/login" >Log in to Update</Button>
	        }

            {/*{ isActive ?
            <Button variant="success" type="submit" id="submitBtn">
            	Submit
            </Button>
            :
            <Button variant="success" type="submit" id="submitBtn">
                Submit
            </Button>
            }*/}
        </Form>
		</>

	)
}