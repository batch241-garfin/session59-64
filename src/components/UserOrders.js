import { useState, useContext } from 'react';

import { Form, Card, Col, Row, Button } from 'react-bootstrap';
import { useNavigate, Navigate } from 'react-router-dom';

import UserContext from '../UserContext';

import Swal from 'sweetalert2';

export default function UserOrders({product}) {

  const { productName, quantity } = product.products[0];

  const [quant, setQuant] = useState(quantity);

  const {user} = useContext(UserContext);

  const navigate = useNavigate();

  const checkOut = () => {

      Swal.fire(
        {
            title: "Checkout Successful",
            icon: "success",
            text: "Your Parcel is on the way!"
        })
        navigate("/products");
    }

  return (

    (user.isAdmin) ?
    <Navigate to="/*"/>
    :
    <Col className="container-fluid bg-success">
        <Row className="cardSize col-md-4 m-auto mb-3">
    <Card className="mt-3 activeCards detailCard">
      <Card.Body className="text-center">
        <Card.Title className="CardTitle mt-4 fontTitle">{productName}</Card.Title>
        <Card.Subtitle className="mb-2 b mt-4">
          Quantity:
        </Card.Subtitle>
        <Form>
        <>
        {/*<label for="quantity">Quantity:</label>*/}
        <input className="text-center b" type="number" id="quantity" name="quantity" min="1" max="10" disabled="true" placeholder={quantity} value={quant} onChange={e => setQuant(e.target.value)}/>
        </>
        </Form>
        <Card.Text>
          
        </Card.Text>

        <Card.Subtitle className="b mt-4">
          Total Amount:
        </Card.Subtitle>
        <Card.Text className="priceBtn">
          PhP {product.totalAmount}.00
        </Card.Text>
        <Button className="bg-primary" onClick={() => checkOut()}>Buy Now!</Button>
      </Card.Body>
    </Card>
    </Row>
    </Col>
  );
}