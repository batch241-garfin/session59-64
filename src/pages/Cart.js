
import '../App.css';
import {useState, useEffect, useContext } from 'react';

import { Navigate } from 'react-router-dom';

import UserContext from '../UserContext';

import UserOrders from '../components/UserOrders';
import TotalAmount from '../components/TotalAmount';

export default function Products(){

	const [products, setProducts] = useState([]);

	const {user} = useContext(UserContext);

	useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/users/userOrder`, {

			headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
		})
		.then(res => res.json())
		.then(data => {

			setProducts(data.map(product => {
				return (
					<UserOrders key={product._id} product ={product} />
				)
			}))
		})
}, []);

	return (
		<>
			{
				(user.isAdmin === true || user.id === null) ?
			    <Navigate to="/*"/>
			    :
			    <>
			    <TotalAmount />
				{products}

				</>
			}
		</>
	)
}