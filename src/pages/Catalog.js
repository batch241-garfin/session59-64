
import {useState, useEffect} from 'react';

import ActiveProducts from '../components/ActiveProducts';

export default function Products(){

	const [products, setProducts] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/activeProducts`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setProducts(data.map(product => {
				return (
					<ActiveProducts key={product.id} product ={product} />
				)
			}))
		})
}, []);

	return (
		<>
		{products}
		</>
	)
}