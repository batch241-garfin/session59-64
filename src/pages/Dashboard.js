
import { useContext } from 'react';

import { Navigate } from 'react-router-dom';

import UserContext from '../UserContext';

// import AllProducts from '../components/AllProducts';
import LabelDashboard from '../components/LabelDashboard';



export default function Products(){

	const {user} = useContext(UserContext);

	const data = {

		title: `Hello! Admin.`,
		destination1: "/",
		destination2: "/newProduct",
		search: "SEARCH",
		add: "NEW PRODUCT",
	}

	// const [products, setProducts] = useState([]);

// 	useEffect(() => {
// 		fetch(`${process.env.REACT_APP_API_URL}/products/allProducts`, {

// 			headers: {
//                 'Content-Type': 'application/json',
//                 Authorization: `Bearer ${localStorage.getItem('token')}`
//             }
// 		})
// 		.then(res => res.json())
// 		.then(data => {
// 			console.log(data)

			// setProducts(data.map(product => {
			// 	return (
			// 		<AllProducts key={product.id} product ={product} />
			// 	)
			// }))
// 		})
// }, []);

	return (

			(user.isAdmin === false || user.id === null) ?
	        <Navigate to="/*"/>
	        :
	        <>
	        {/*<SearchProduct/>*/}
	        <LabelDashboard data={data}/>
	        {/*<AllProducts/>*/}
			{/*{products}*/}
			</>
	)
}