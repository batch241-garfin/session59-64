import Banner from '../components/Banner';
//import Highlights from '../components/Highlights';


export default function Home()
{

	const data = {

		title: "Welcome Hero!",
		content: "Yes, Milord? Orders? What do you need?",
		subTitle: "For Lordaeron!",
		destination: "/products",
		label: "Equip yourself!"
	}

	return (

		<>
			<Banner data={data}/>
     		{/*<Highlights />*/}
		</>
	)
}