
import '../App.css';
import { useState, useEffect, useContext } from 'react';

import { Form, Button } from 'react-bootstrap';

import { Navigate } from 'react-router-dom';

import UserContext from '../UserContext';

import Swal from 'sweetalert2';


export default function Login() {

    const {user, setUser} = useContext(UserContext);

	const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);


    function loginUser(e)
    {
    	e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {

            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if (typeof data.access !== "undefined")
            {

                localStorage.setItem('token', data.access)
                retrieveUserDetails(data.access);

                console.log(localStorage);

                Swal.fire(
                {
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to Zuitt!"
                })
                }
                else
                {
                Swal.fire(
                {
                    title: "Authentication Failed",
                    icon: "error",
                    text: "Please, check your login details and try again."
                })
            }
        });

        			setEmail('');
        			setPassword('');
    }

    const retrieveUserDetails = (token) => {

        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {

            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    };

    useEffect(() => {

        if(email !== '' && password !== '')
        {
            setIsActive(true);
            // document.querySelector('#submitBtn').removeAttribute('disabled', true);
        }
        else
        {
            setIsActive(false);
            // document.querySelector('#submitBtn').setAttribute('disabled', true);
        }
    }, [email, password]);


    return (

        (user.id !== null) ?
        <Navigate to="/"/>
        :
        <Form className="regForm pb-4 p-5" onSubmit={(e) => loginUser(e)} >
            <Form.Group controlId="userEmail">
                <Form.Label className="b">Email Address:</Form.Label>
                <Form.Control 
	                type="email" 
	                placeholder="Enter email"
                    value={email}
                    onChange={e => setEmail(e.target.value)}
	                required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label className="b">Password:</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Password"
                    value={password}
                    onChange={e => setPassword(e.target.value)}
	                required
                />
            </Form.Group>

            { isActive ?
            <Button className="succBtn mt-3 b regBtn" type="submit" id="submitBtn">
            	Login
            </Button>
            :
            <Button className="succBtn mt-3 b regBtn" type="submit" id="submitBtn">
                Login
            </Button>
            }
        </Form>
    )
}