import { useState, useEffect, useContext } from 'react';

import { Form, Button } from 'react-bootstrap';

import { Navigate } from 'react-router-dom';

import UserContext from '../UserContext';

import { Link } from 'react-router-dom';

import Swal from 'sweetalert2';


export default function NewProduct() {

    const {user} = useContext(UserContext);

	const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState('');
    const [isActive, setIsActive] = useState(false);


    function newProduct(e)
    {
    	e.preventDefault()


        fetch(`${process.env.REACT_APP_API_URL}/products/add`, {

            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price
            })
        })
        .then(res => res.json())
        .then(data => {
console.log(data)

            if (data !== false)
            {
                Swal.fire(
                {
                    title: "Successfully Added a New Product",
                    icon: "success",
                    text: "Let's try to add new product!"
                })
            }
            else
            {
                Swal.fire(
                {
                    title: "No product has been added!",
                    icon: "error",
                    text: "Please, check your product details and try again."
                })
            }
        });

        	setName('');
    		setDescription('');
            setPrice('');
    }

    useEffect(() => {

        if(name !== '' && description !== '' && price !== '')
        {
            setIsActive(true);
            // document.querySelector('#submitBtn').removeAttribute('disabled', true);
        }
        else
        {
            setIsActive(false);
            // document.querySelector('#submitBtn').setAttribute('disabled', true);
        }
    }, [name, description, price]);


    return (

        (user.isAdmin === false || user.id === null) ?
        <Navigate to="/*"/>
        :
        <>
        <Form className="regForm pb-4 p-5" onSubmit={(e) => newProduct(e)} >
            <Form.Group controlId="userEmail">
                <Form.Label className="b">Product Name:</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Enter Product"
                    value={name}
                    onChange={e => setName(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="description">
                <Form.Label className="b">Description:</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Enter Description"
                    value={description}
                    onChange={e => setDescription(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="price">
                <Form.Label className="b">Price:</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Enter Price"
                    value={price}
                    onChange={e => setPrice(e.target.value)}
                    required
                />
            </Form.Group>

            <Button className="mLeft mt-3" as={Link} to="/dashboard">Go Back</Button>

            { isActive ?
            <Button className="succBtns mt-3 mLeft b" type="submit" id="submitBtn">
                Submit
            </Button>
            :
            <Button className="succBtns b mt-3 mLeft" type="submit" id="submitBtn">
                Submit
            </Button>
            }
        </Form>
        </>
    )
}