import { useState, useEffect, useContext } from 'react';

import React from 'react';

import { Form, Button } from 'react-bootstrap';

import { Navigate } from 'react-router-dom';

import UserContext from '../UserContext';

import Swal from 'sweetalert2';

//import { AxiosContext } from 'react-axios/lib/components/AxiosProvider'

//import axios from "axios"


export default function NewProduct() {

    const {user, setUser} = useContext(UserContext);

	const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);
    const [image, setImage] = useState(false);


    function addImage(e)
    {
    	e.preventDefault()

        console.log(e.target.files);
        setImage(e.target.files[0])

        
    }

    function handleApi()
    {
        const formData = new FormData()
        formData.append('image', image)
         // axios.post('/', formData).then((res) => {

         //    console.log(res)
         // })
    }



    useEffect(() => {

        if(email !== '' && password !== '')
        {
            setIsActive(true);
            // document.querySelector('#submitBtn').removeAttribute('disabled', true);
        }
        else
        {
            setIsActive(false);
            // document.querySelector('#submitBtn').setAttribute('disabled', true);
        }
    }, [email, password]);


    return (

        <div>
            <input type="file" name="file" onChange={addImage} />
            <button onClick={handleApi} >Submit</button>
        </div>
    )
}