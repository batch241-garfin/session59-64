
import '../App.css';

import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';

import UserContext from '../UserContext';

import Swal from 'sweetalert2';

export default function Register() {

    const {user} = useContext(UserContext);

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [mobNum, setMobNum] = useState('');

    const [isActive, setIsActive] = useState(false);

    function registerUser(e)
    {
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/users/register`, {

            method: 'POST',
            headers: {
           'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                email: email,
                mobileNo: mobNum,
                password: password1
                })
            })
            .then(res => res.json())
            .then(data => {
                console.log(data)

                if (data === false)
                {
                    Swal.fire(
                    {
                        title: "Duplicate email found",
                        icon: "error",
                        text: "Please provide a different email."
                    })
                }
                else
                {
                    Swal.fire(
                    {
                        title: "Registration successful",
                        icon: "success",
                        text: "Welcome to Zuitt!"
                    })

                    // Clears the form
                    setFirstName('');
                    setLastName('');
                    setEmail('');
                    setMobNum('');
                    setPassword1('');
                    setPassword2('');
                }
            });
    }
    

    useEffect(() => {

        if(email !== '' && firstName !== '' && lastName !== '' && mobNum !== '' && mobNum.length === 11 && password1 !== '' && password2 !== '' && password1 === password2)
        {
            setIsActive(true);
            // document.querySelector('#submitBtn').removeAttribute('disabled', true);
        }
        else
        {
            setIsActive(false);
            // document.querySelector('#submitBtn').setAttribute('disabled', true);
        }
    }, [email, firstName, lastName, mobNum, password1, password2]);

    return (

        (user.id !== null) ?
        <Navigate to="/products"/>
        :
        <Form className="regForm pb-4 p-5" onSubmit={(e) => registerUser(e)} >
            <Form.Group controlId="userFirstName">
                <Form.Label className="b">First Name:</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Enter Firstname"
                    value={firstName}
                    onChange={e => setFirstName(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="userLastName" className="">
                <Form.Label className="b">Last Name:</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Enter Lastname"
                    value={lastName}
                    onChange={e => setLastName(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="userEmail">
                <Form.Label className="b">Email address</Form.Label>
                <Form.Control 
	                type="email" 
	                placeholder="Enter email"
                    value={email}
                    onChange={e => setEmail(e.target.value)}
	                required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="userMobNum">
                <Form.Label className="b">Mobile Number:</Form.Label>
                <Form.Control 
                    type="Number" 
                    placeholder="Enter Mobile Number"
                    value={mobNum}
                    onChange={e => setMobNum(e.target.value)}
                    minLength="11"
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label className="b">Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Password"
                    value={password1}
                    onChange={e => setPassword1(e.target.value)}
	                required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label className="b">Verify Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Verify Password"
                    value={password2}
                    onChange={e => setPassword2(e.target.value)}
	                required
                />
            </Form.Group>

            { isActive ?
            <Button className="b text-center mt-3 regBtn succBtn" variant="primary" type="submit" id="submitBtn">
            	Register
            </Button>
            :
            <Button className="b text-center mt-3 regBtn errBtn" variant="danger" type="submit" id="submitBtn">
                Register
            </Button>
            }
        </Form>
    )
}